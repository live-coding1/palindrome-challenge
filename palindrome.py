# reverse text by reading from the end
# using the negative index
def reverse(text):
    return text[::-1]

def is_palindrome(text):
    # delete empty space
    # put the text in the lower case
    # replace all spaces by an empty character
    text = text.strip().lower().replace(' ', '')

    if text == '':
        return False

    # now I will check if text is palindrome
    # by using slice
    if text == reverse(text):
        return True
    # otherwise the text is not Palindrome
    return False

# tests cases
print(is_palindrome('') == False)

print(is_palindrome('kayak') == True)

print(is_palindrome('Ten animals I slam in a net') == True)

print(is_palindrome('Eleven animals I slam in a net') == False)

# That is all

# If you like this video please subscribe and share :)

# see you soon